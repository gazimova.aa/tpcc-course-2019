# Работа через Docker

С помощью докера вы сможете развернуть контейнер со всем необходимым окружением: компиляторами, линтерами, пакетами.

## Заводим контейнер

1. Установите ```docker```. Если он уже установлен, переходите к следующему пункту. 
В ```Ubuntu``` — ```sudo apt install docker.io```, иначе [смотрите в руководстве](https://docs.docker.com/get-started/).
2. В большинстве систем для работы с ```docker``` требуются права суперпользователя. Если вам не хочется каждый раз вводить ```sudo```, то создайте группу пользователей, которые имеют расширенные права при работе с ```docker```, и добавьте в неё себя, как показано ниже, после чего перелогиньтесь, чтобы обновления вступили в силу.
    ```
    sudo groupadd docker
    sudo gpasswd -a $USER docker
    newgrp docker
    ```
3. Проверьте корректность установки с помощью следующей команды:
   ```
   docker run hello-world
   ```
4. Заведите директорию для работы с курсом, она понадобится чуть позже.
   ```
   mkdir tpcc
   ```
5. Скачайте директорию ```docker``` из репозитория курса.
6. Перейдите в скачанную папку и начните установку образа:
   ``` 
   docker build -t tpcc .
   ```
7.  Создайте докер-контейнер: 
   ```
   docker run -v ~/tpcc:/root --cap-add SYS_PTRACE -it tpcc

   ```

Теперь можно клонировать репозиторий и начинать работу.

Все установленные файлы будут доступны как из контейнера, так и из хостовой системы в директории ```~/tpcc```.

Опция `--cap-add SYS_PTRACE` [нужна](https://github.com/google/sanitizers/issues/916) для корректной работы Leak Sanitizer-а.

## Работаем с контейнером

1. Найдите id контейнера:
    ```
    docker ps -a
    ```
2. Если контейнер остановлен, то запустите его:
    ```
    docker start %container_id%
    ```
    Чтобы открылся терминал, добавьте флаг `-i`.
3. Запустите терминал для работы:
    ```
    docker exec -it %container_id% /bin/bash
    ```

