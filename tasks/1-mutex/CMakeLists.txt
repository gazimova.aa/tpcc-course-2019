cmake_minimum_required(VERSION 3.9)

add_subdirectory(mutex)
add_subdirectory(spinlock)
add_subdirectory(toyalloc)
add_subdirectory(tricky)
add_subdirectory(try-lock)

