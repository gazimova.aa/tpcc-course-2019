#pragma once

#include <mutex>

class StdMutex {
 public:
  void Lock() {
    impl_.lock();
  }

  void Unlock() {
    impl_.unlock();
  }  

 private:
  std::mutex impl_;
};
