#pragma once

#include "thread_local.hpp"

#include <twist/stdlike/atomic.hpp>
#include <twist/threading/spin_wait.hpp>

namespace solutions {

class RCULock {
 public:
  void ReadLock() {
    // Your code goes here
  }

  void ReadUnlock() {
    // Your code goes here
  }

  void Synchronize() {
    // Your code goes here
  }

  void lock_shared() {  // NOLINT
    ReadLock();
  }

  void unlock_shared() {  // NOLINT
    ReadUnlock();
  }
};

}  // namespace solutions
