#pragma once

#include <twist/stdlike/atomic.hpp>
#include <twist/threading/spin_wait.hpp>

#include <memory>
#include <thread>
#include <vector>

namespace logging {

struct LogEvent {
  std::thread::id id_;
  std::string message_;

  LogEvent(std::string message)
      : id_(std::this_thread::get_id()), message_(std::move(message)) {
  }
};

using LogEvents = std::vector<LogEvent>;

class ILogWriter {
 public:
  virtual ~ILogWriter() = default;

  // Blocking
  virtual void Write(const LogEvents& events) = 0;
};

using ILogWriterPtr = std::shared_ptr<ILogWriter>;

class Logger {
 public:
  Logger(ILogWriterPtr writer)
      : writer_(std::move(writer)) {
    // Not implemented
  }

  void Log(std::string message) {
    // Not implemented
  }

  void Synchronize() {
    // Not implemented
  }

  void Stop() {
    // Not implemented
  }

 private:
  ILogWriterPtr writer_;
};

}  // namespace logging
