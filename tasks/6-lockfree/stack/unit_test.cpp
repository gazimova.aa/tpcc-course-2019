#include "lfstack.hpp"

#include <twist/test_framework/test_framework.hpp>
#include <twist/threading/test.hpp>

#include <iostream>
#include <string>

template <typename T>
using Stack = solutions::LockFreeStack<T>;

TEST_SUITE(LockFreeStack) {
  SIMPLE_T_TEST(InitiallyEmpty) {
    Stack<int> stack;
    int item;
    ASSERT_FALSE(stack.Pop(item));
  }

  SIMPLE_T_TEST(PushThenPop) {
    Stack<std::string> stack;

    stack.Push("Hello");

    std::string item;
    ASSERT_TRUE(stack.Pop(item));
    ASSERT_EQ(item, "Hello");

    ASSERT_FALSE(stack.Pop(item));
  }

  SIMPLE_T_TEST(Lifo) {
    static const int kInts = 100;

    Stack<int> ints;

    for (int i = 0; i < kInts; ++i) {
      ints.Push(i);
    }

    int next_int;
    for (int i = 0; i < kInts; ++i) {
      ASSERT_TRUE(ints.Pop(next_int));
      ASSERT_EQ(next_int, kInts - 1 - i);
    }

    // is empty?
    ASSERT_FALSE(ints.Pop(next_int));
  }

  struct TestObject {
    static size_t ctor_count_;
    static size_t dtor_count_;

    TestObject() {
      ++ctor_count_;
    }

    TestObject(const TestObject& /*that*/) {
      ++ctor_count_;
    }

    TestObject(TestObject&& /*that*/) {
      ++ctor_count_;
    }

    TestObject& operator=(TestObject&& that) = default;
    TestObject& operator=(const TestObject& that) = default;

    ~TestObject() {
      ++dtor_count_;
    }

    static void ResetCounters() {
      ctor_count_ = 0;
      dtor_count_ = 0;
    }
  };

  size_t TestObject::ctor_count_ = 0;
  size_t TestObject::dtor_count_ = 0;

  SIMPLE_T_TEST(PushRelease) {
    TestObject::ResetCounters();
    {
      Stack<TestObject> stack;
      stack.Push(TestObject{});
    }
    ASSERT_EQ(TestObject::dtor_count_, TestObject::ctor_count_);
  }

  SIMPLE_T_TEST(PushPopRelease) {
    TestObject::ResetCounters();
    {
      Stack<TestObject> stack;
      stack.Push(TestObject{});
      TestObject popped;
      ASSERT_TRUE(stack.Pop(popped));
    }

    ASSERT_EQ(TestObject::dtor_count_, TestObject::ctor_count_);
  }

  SIMPLE_T_TEST(Cleanup) {
    TestObject::ResetCounters();

    {
      Stack<TestObject> stack;
      stack.Push(TestObject{});
      stack.Push(TestObject{});
      stack.Push(TestObject{});
    }

    ASSERT_EQ(TestObject::dtor_count_, TestObject::ctor_count_);
  }

  struct SingleDestructable {

    volatile bool not_destructed;
    int value;
    // Default allocator zeroes memory after destruction. Thus store true in not_destructed
    explicit SingleDestructable(int v) : not_destructed(true), value(v) {
      // noop
    };

    ~SingleDestructable() {
      ASSERT_TRUE(not_destructed);
      not_destructed = false;
    }
  };

  SIMPLE_T_TEST(DoubleDestructionTest) {
    Stack<SingleDestructable> stack;
    stack.Push(SingleDestructable{1});
    stack.Push(SingleDestructable{2});
    SingleDestructable popped{0};
    ASSERT_TRUE(stack.Pop(popped));
    ASSERT_EQ(popped.value, 2);
  }
}

RUN_ALL_TESTS()
