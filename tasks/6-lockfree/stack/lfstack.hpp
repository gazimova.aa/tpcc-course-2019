#pragma once

#include "stamped_ptr.hpp"

#include <twist/stdlike/atomic.hpp>
#include <twist/support/compiler.hpp>

namespace solutions {

// Treiber lock-free stack

template <typename T>
class LockFreeStack {
 public:
  LockFreeStack() {
    // Not implemented
  }

  ~LockFreeStack() {
    // Not implemented
  }

  void Push(T item) {
    UNUSED(item);
    // Not implemented
  }

  bool Pop(T& item) {
    UNUSED(item);
    return false;  // Not implemented
  }
};

}  // namespace solutions

