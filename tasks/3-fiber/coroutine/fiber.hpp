#pragma once

#include "coroutine.hpp"

namespace fiber {

using Routine = coro::Routine;

void RunScheduler(Routine main);

void Spawn(Routine routine);
void Yield();

}  // namespace fiber
