#include "scheduler.hpp"

#include <twist/support/compiler.hpp>
#include <twist/support/random.hpp>
#include <twist/support/time.hpp>

#include <thread>

namespace fiber {

//////////////////////////////////////////////////////////////////////

static thread_local Fiber* current_fiber;

Fiber* GetCurrentFiber() {
  return current_fiber;
}

static inline Fiber* GetAndResetCurrentFiber() {
  auto* f = current_fiber;
  current_fiber = nullptr;
  return f;
}

static inline void SetCurrentFiber(Fiber* f) {
  current_fiber = f;
}

//////////////////////////////////////////////////////////////////////

static thread_local Scheduler* current_scheduler;

static inline Scheduler* GetCurrentScheduler() {
  VERIFY(current_scheduler, "Not in fiber context");
  return current_scheduler;
}

struct SchedulerScope {
  SchedulerScope(Scheduler* scheduler) {
    VERIFY(!current_scheduler, "Cannot run scheduler from another scheduler");
    current_scheduler = scheduler;
  }

  ~SchedulerScope() {
    current_scheduler = nullptr;
  }
};

//////////////////////////////////////////////////////////////////////

TimePoint Now() {
  return std::chrono::steady_clock::now();
}

TimePoint ToDeadLine(Duration duration) {
  return Now() + duration;
}

//////////////////////////////////////////////////////////////////////

Scheduler::Scheduler() {
}

// Operations invoked by running fibers

void Scheduler::SwitchToScheduler() {
  Fiber* caller = GetAndResetCurrentFiber();
  caller->Context().SwitchTo(context_);
}

void Scheduler::Spawn(FiberRoutine routine) {
  auto* created = CreateFiber(routine);
  Schedule(created);
}

void Scheduler::Yield() {
  Fiber* caller = GetCurrentFiber();
  caller->SetState(FiberState::Runnable);
  SwitchToScheduler();
}

void Scheduler::SleepFor(Duration delay) {
  twist::Timer timer;
  do {
    Yield();
  } while (timer.Elapsed() < delay);
}

void Scheduler::Terminate() {
  Fiber* caller = GetCurrentFiber();
  caller->SetState(FiberState::Terminated);
  SwitchToScheduler();
}

// Scheduling

void Scheduler::Run(FiberRoutine main) {
  SchedulerScope scope(this);
  Spawn(main);
  RunLoop();
}

void Scheduler::RunLoop() {
  while (!run_queue_.IsEmpty()) {
    Fiber* next = run_queue_.PopFront();
    SwitchTo(next);
    Reschedule(next);
  }
}

void Scheduler::SwitchTo(Fiber* fiber) {
  SetCurrentFiber(fiber);
  fiber->SetState(FiberState::Running);
  // scheduler context_ -> fiber->context_
  context_.SwitchTo(fiber->Context());
}

void Scheduler::Reschedule(Fiber* fiber) {
  switch (fiber->State()) {
    case FiberState::Runnable:  // From Yield
      Schedule(fiber);
      break;
    case FiberState::Terminated:  // From Terminate
      Destroy(fiber);
      break;
    default:
      PANIC("Unexpected fiber state");
      break;
  }
}

void Scheduler::Schedule(Fiber* fiber) {
  run_queue_.PushBack(fiber);
}

Fiber* Scheduler::CreateFiber(FiberRoutine routine) {
  return Fiber::Create(routine);
}

void Scheduler::Destroy(Fiber* fiber) {
  delete fiber;
}

//////////////////////////////////////////////////////////////////////

void RunScheduler(FiberRoutine main) {
  Scheduler scheduler;
  scheduler.Run(main);
}

void Spawn(FiberRoutine routine) {
  GetCurrentScheduler()->Spawn(std::move(routine));
}

void Yield() {
  GetCurrentScheduler()->Yield();
}

void SleepFor(Duration delay) {
  GetCurrentScheduler()->SleepFor(delay);
}

void Terminate() {
  GetCurrentScheduler()->Terminate();
}

}  // namespace fiber
